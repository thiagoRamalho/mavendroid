package br.com.mavendroid;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.app.Activity;
import android.widget.Button;


@RunWith(RobolectricTestRunner.class)
public class HelloAndroidActivityTest {

  @org.junit.Test
  public void testSomething() throws Exception {
    Activity activity = Robolectric.buildActivity(HelloAndroidActivity.class).create().get();
    assertTrue(activity != null);
    Button b = (Button) activity.findViewById(R.id.button1);
    assertEquals("Button", b.getText());
  }
}